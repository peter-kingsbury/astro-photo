# Shooting Tips

1. Pick an (ideally) dark night.
   * Check [Moon Phases](https://www.timeanddate.com/moon/phases/)
   * Check [Clear Sky Chart](http://www.cleardarksky.com/c/CmbrdgOnkey.html?1) 
1. Choose the subject
   * Check [Sea and Sky Astronomy Calendar](http://www.seasky.org/astronomy/astronomy-calendar-current.html)
   * Check SkyView app
   - e.g. Orion nebula during New Moon in winter.
1. Find the sweet spot.
   - Calculate maximum shutter-time based on focal length.
1. Use ideal f-ratio between f/2 and f/4.
1. Shoot bulb or delay-timer images.
1. Plan for 10+ photos.
1. Use the exposure histogram during discovery-process to find ideal settings.
   - Choose exposure time, then ISO, then aperture.
   - Shoot at lowest ISO possible.
1. Stack the exposures.

## Camera Settings

- **Mode** - Manual
- **Aperture** - f/3.5
- **ISO** - 800-1600-3200
- **White Balance** - Auto
- **Focus** - Manual
- **Exposure** - Based on lens focal length (see [exposure times](exposure_times.md))
