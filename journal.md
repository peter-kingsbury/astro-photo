# Journal

## 2021-Jan-29

Read [FreeAstro.org tutorial](https://free-astro.org/) on Siril usage. Tried stacking some unintentionally-taken photos but need more stackable subjects.

## 2021-Jan-27

iCSC indicated clear skies tonight.

Planned to go outside around 7pm. Clouded over. Went back inside.

Checked outside again before bedtime for S&G. Perfectly clear sky. FML.
