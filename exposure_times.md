# Exposure Times

## Omegon Minitrack LX2

([manual](https://www.dalekohledy-mikroskopy.cz/static/9/95ad063083feab-omegon-mini-track-lx2-an.pdf))

**Time (min) = 100 / Objective focal length (mm)**

(For setups up to 2kg -- heavier setups require shorter exposure times.)

| Objective Focal Length | Maximum Exposure     |
| ---------------------- | -------------------- |
| 18mm                   | 5 minutes 30 seconds |
| 50mm                   | 2 minutes            |
| 55mm                   | 1 minute 49 seconds  |
| 100mm                  | 1 minute             |
