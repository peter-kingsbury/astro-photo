# 🌌 Astro Photo

## Introduction
My name is Peter Kingsbury, and I'm interested in astrophotography.

This repository serves multiple purposes:
1. 📜 Document my development as an avid astrophotography fan.
1. 🖼️ Serve as a showcase for the photos I take. 
1. ✍️ Serve as a journal during my continued self-education.

## Equipment
The equipment I regularly use is listed below.

### Hardware

#### Star Tracking
- [Omegon Minitrack LX2](https://www.omegon.eu/camera-mounts/omegon-mount-mini-track-lx2/p,55040) ([manual](https://www.dalekohledy-mikroskopy.cz/static/9/95ad063083feab-omegon-mini-track-lx2-an.pdf))
- [Pergear TH4 Ball Head](https://www.pergear.com/products/pergear-th4-ball-head-th3-tripod)

#### Cameras
- Canon EOS Rebel T3

#### Lenses
- Canon EF-S 18-55mm IS

#### Accessories
- Rocketfish Neutral Density filter ***not used**
- Hoya UV(c) filter ***not used**
- Lens hood (petal)
- 🔦 [EverBrite LED headlamp](https://www.amazon.ca/gp/product/B08975WM6Y/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1) 

### Software

#### Computer
- 🐧 [Linux Mint](https://linuxmint.com/)
- 🔭 [Siril](https://www.siril.org/)
- 🖌️ [Krita](https://krita.org/en/)

#### Phone
- SkyView app (iOS)
- iSCS app (iOS)
- Compass app (iOS)

## Copyright Notice

All images © 2021 Peter Kingsbury. All rights reserved.

Use of images found within this repository are subject to copyright law, and may not be used in whole or in part without express written consent by the owner.
